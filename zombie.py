# Benson Liu
# bliu13@ucsc.edu
#
# CMPS 5P, Spring 2014
# Assignment 8
#
# This program simulates a zombie apocalypse and what happens visually
# to the healthy and infected when a population when there is an outbreak.

__author__ = 'bliu13'


from random import random


def check_prob(prob):
    """
    The parameter, prob is probability of disease spread, birth and etc...
    It is compared to randomly generated number. Will return either True
    or False.
    """
    random_num = random()
    if random_num < prob:
        return True
    else:
        return False


def print_grid(size_n, grid):
    """
    Prints current grid line by line to standard out.
    """

    for row in range((size_n - 1), -1, -1):
        # Want grid to start at bottom left corner so we need the highest index for row
        # to be printed 'last', hence going from bottom to top.
        row_str = ''
        for column in range(0, size_n):
            # Converts the number in the grid into a string for printing
            row_str += str(grid[(row, column)])

            if column is not (size_n - 1):
                # Prints a space if it is not the last column
                row_str += ' '
        print(row_str)


def create_empty_grid(size_n):
    """
    Creates an empty grid.
    """
    grid = dict()
    for x in range(0, size_n):
        for y in range(0, size_n):
            grid[(x, y)] = '.'
    return grid


def initial_population(size_n, grid, population_density):
    """
    Will take in a population density value between 0 and 1 and we
    pass it in to check_prob() and if it returns true then the cell
    is occupied. This is only used at the beginning of the simulation
    after the grid has been created by using the function
    create_empty_grid().

    Will modify the grid that was passed in as one of its parameters so
    there is no need to return anything.
    """
    for row in range((size_n - 1), -1, -1):
        # Want grid to start at bottom left corner so we need the highest index for row
        # to be printed 'last', hence going from bottom to top.
        for column in range(0, size_n):
            if check_prob(population_density) is True:
                grid[(row, column)] = 0


def initial_infected_population(size_n, grid, infected_chance):
    """
    Will take in a disease chance value between 0 and 1 and we pass it
    in to check_prob() and if it returns true then existing cell is infected.
    This is only used at the beginning of the simulation after the grid
    has been created by using the function create_empty_grid() and then
    calling seed_population().

    Will modify the grid that was passed in as one of its parameters so
    there is no need to return anything.
    """
    for row in range((size_n - 1), -1, -1):
        # Want grid to start at bottom left corner so we need the highest index for row
        # to be printed 'last', hence going from bottom to top.
        for column in range(0, size_n):
            if (grid[(row, column)] is 0) and (check_prob(infected_chance) is True):
                grid[(row, column)] = 1


def check_boundary(size_n, row, column):
    """
    Give the parameters size_n, which is the size of the row and
    column of the n x n matrix, and the row and column coordinate,
    we return a dictionary that tells the user if the current
    coordinate has a row below and on top of it as well as if
    there is a column to the right and left of it.
    """
    boundary_dict = dict()

    # Checks what 'boundaries' the existing cell can work on
    boundary_dict['Top'] = False
    boundary_dict['Bottom'] = False
    boundary_dict['Right'] = False
    boundary_dict['Left'] = False
    if (row + 1) < size_n:
        # If there is a row on top of the current element
        boundary_dict['Top'] = True
    if (row - 1) >= 0:
        # If there is a row below the current element
        boundary_dict['Bottom'] = True
    if (column + 1) < size_n:
        # If there is a column on top of the current element
        boundary_dict['Right'] = True
    if (column - 1) >= 0:
        # If there is a column on top of the current element
        boundary_dict['Left'] = True

    return boundary_dict


def new_birth(size_n, grid_today, grid_tomorrow, chance_of_birth):
    """
    Will take in a birth chance value between 0 and 1 and we pass it
    in to check_prob() and if it returns true then an empty space around
    an existing healthy cell.

    Function works by scanning grid_today for healthy cells one at a time
    and then checks the probability of new cell birth using the above
    mentioned method. The more healthy cells around a particular spot in
    the grid, the more likely that there will be a new cell birthed there.
    If a new healthy cell arises, it will be put into grid_tomorrow, leaving
    grid_today untouched.

    Visualization of the elements in matrix affected where 'X' is the
    existing healthy cell we are looking at and 'N' is the space that
    a potential new cell will arise at.
            N N N
            N X N
            N N N

    Will modify tomorrow_grid.
    """

    # The two for loops will scan the matrix for healthy cells
    for row in range((size_n - 1), -1, -1):
        # Want grid to start at bottom left corner so we need the highest index for row
        # to be printed 'last', hence going from bottom to top.
        for column in range(0, size_n):
            if (isinstance(grid_today[(row, column)], int)) and (grid_today[(row, column)] is 0):
                # Checks if the element in the grid is an integer and not a string. This is
                # necessary is because it is either a number to represent a healthy or infected
                # cell or it is an empty spot then it would be a '.' which is a string.

                # Checks what 'boundaries' the existing cell can work on. It will only change
                # the empty element if the matrix element contains '.'
                boundary = check_boundary(size_n, row, column)

                if boundary['Top'] is True:
                    # Tries to modify element on Top
                    if check_prob(chance_of_birth) is True:
                        if grid_today[(row + 1, column)] is '.':
                            grid_tomorrow[(row + 1, column)] = 0

                    # Tries to modify Top Left and Top Right
                    if (boundary['Left'] is True) and (check_prob(chance_of_birth) is True):
                        if grid_today[(row + 1, column - 1)] is '.':
                            grid_tomorrow[(row + 1, column - 1)] = 0
                    if (boundary['Right'] is True) and (check_prob(chance_of_birth) is True):
                        if grid_today[(row + 1, column + 1)] is '.':
                            grid_tomorrow[(row + 1, column + 1)] = 0

                if boundary['Bottom'] is True:
                    # Tries to modify element on Bottom
                    if check_prob(chance_of_birth) is True:
                        if grid_today[(row - 1, column)] is '.':
                            grid_tomorrow[(row - 1, column)] = 0

                    # Tries to modify Bottom Left and Bottom Right
                    if (boundary['Left'] is True) and (check_prob(chance_of_birth) is True):
                        if grid_today[(row - 1, column - 1)] is '.':
                            grid_tomorrow[(row - 1, column - 1)] = 0
                    if (boundary['Right'] is True) and (check_prob(chance_of_birth) is True):
                        if grid_today[(row - 1, column + 1)] is '.':
                            grid_tomorrow[(row - 1, column + 1)] = 0

                # Tries to modify element on Left
                if (boundary['Left'] is True) and (check_prob(chance_of_birth) is True):
                    if grid_today[(row, column - 1)] is '.':
                        grid_tomorrow[(row, column - 1)] = 0

                # Tries to modify element on Left
                if (boundary['Right'] is True) and (check_prob(chance_of_birth) is True):
                    if grid_today[(row, column + 1)] is '.':
                        grid_tomorrow[(row, column + 1)] = 0


def new_infected(size_n, grid_today, grid_tomorrow, chance_of_spread):
    """
    Will take in a spread chance value between 0 and 1 and we pass it
    in to check_prob() and if it returns true then a health cell
    around an existing healthy cell will be infected.

    Function works by scanning grid_today for infected cells one at a time
    and then checks the probability of new infected cell using the above
    mentioned method. The more infected cells around a healthy cell in the
    grid, the more likely that the cell will be infected. If a healthy cell
    is infected, it will be put the changes into grid_tomorrow, leaving
    grid_today untouched.

    Visualization of the elements in matrix affected where 'X' is the
    existing healthy cell we are looking at and 'N' is the space that
    an existing healthy cell will be infected.
            N N N
            N X N
            N N N

    Will modify tomorrow_grid.
    """
    # The two for loops will scan the matrix for healthy cells
    for row in range((size_n - 1), -1, -1):
        # Want grid to start at bottom left corner so we need the highest index for row
        # to be printed 'last', hence going from bottom to top.
        for column in range(0, size_n):
            if (isinstance(grid_today[(row, column)], int)) and (grid_today[(row, column)] >= 1):
                # Checks if the element in the grid is an integer and not a string. This is
                # necessary is because it is either a number to represent a healthy or infected
                # cell or it is an empty spot then it would be a '.' which is a string.

                # Checks what 'boundaries' the existing cell can work on. It will only change
                # the empty element if the matrix element contains 0
                boundary = check_boundary(size_n, row, column)

                if boundary['Top'] is True:
                    # Tries to modify element on Top
                    if check_prob(chance_of_spread) is True:
                        if grid_today[(row + 1, column)] is 0:
                            grid_tomorrow[(row + 1, column)] = 1

                    # Tries to modify Top Left and Top Right
                    if (boundary['Left'] is True) and (check_prob(chance_of_spread) is True):
                        if grid_today[(row + 1, column - 1)] is 0:
                            grid_tomorrow[(row + 1, column - 1)] = 1
                    if (boundary['Right'] is True) and (check_prob(chance_of_spread) is True):
                        if grid_today[(row + 1, column + 1)] is 0:
                            grid_tomorrow[(row + 1, column + 1)] = 1

                if boundary['Bottom'] is True:
                    # Tries to modify element on Bottom
                    if check_prob(chance_of_spread) is True:
                        if grid_today[(row - 1, column)] is 0:
                            grid_tomorrow[(row - 1, column)] = 1

                    # Tries to modify Bottom Left and Bottom Right
                    if (boundary['Left'] is True) and (check_prob(chance_of_spread) is True):
                        if grid_today[(row - 1, column - 1)] is 0:
                            grid_tomorrow[(row - 1, column - 1)] = 1
                    if (boundary['Right'] is True) and (check_prob(chance_of_spread) is True):
                        if grid_today[(row - 1, column + 1)] is 0:
                            grid_tomorrow[(row - 1, column + 1)] = 1

                # Tries to modify element on Left
                if (boundary['Left'] is True) and (check_prob(chance_of_spread) is True):
                    if grid_today[(row, column - 1)] is 0:
                        grid_tomorrow[(row, column - 1)] = 1

                # Tries to modify element on Left
                if (boundary['Right'] is True) and (check_prob(chance_of_spread) is True):
                    if grid_today[(row, column + 1)] is 0:
                        grid_tomorrow[(row, column + 1)] = 1


def infected_mortality(size_n, grid_today, grid_tomorrow, rate_of_mortality, duration_of_disease):
    """
    Will take in a mortality rate value between 0 and 1 and we pass it
    in to check_prob() and if it returns true then an infected cell
    will die. This function only operates on cells who have already
    reached the full disease duration.

    Function works by scanning grid_today for infected cells one at a time
    and then checks if the infected cell has reached the required disease
    duration. If it has then this function will check the probability of
    the cell dying or recovering. If the infected cell dies or becomes healthy
    it will be put the changes into grid_tomorrow, leaving grid_today untouched.

    Will modify grid_tomorrow.
    """
    # The two for loops will scan the matrix for infected cells
    for row in range((size_n - 1), -1, -1):
        # Want grid to start at bottom left corner so we need the highest index for row
        # to be printed 'last', hence going from bottom to top.
        for column in range(0, size_n):
            if (isinstance(grid_today[(row, column)], int)) and (grid_today[(row, column)] is duration_of_disease):
                # Checks if the element in the grid is an integer and not a string. This is
                # necessary is because it is either a number to represent a healthy or infected
                # cell or it is an empty spot then it would be a '.' which is a string.
                if check_prob(rate_of_mortality) is True:
                    # If infection is fatal
                    grid_tomorrow[(row, column)] = '.'
                else:
                    # Cell survives infection and heals
                    grid_tomorrow[(row, column)] = 0


def increment_infected_duration(size_n, grid_tomorrow):
    """
    Will take in tomorrow_grid and increments all the infected cell's
    number of days that they were infected for. Out of all the functions
    that modify the tomorrow_grid, this one should be ran first because
    it does not add in new cells or destroy infected cells but merely
    increments the count on infected cells.

    Function works by scanning for infected cells one at a time and then
    checks if the infected cell has reached the required disease duration.
    If it has then this function will check the probability of the cell
    dying or recovering.

    Will modify grid_tomorrow.
    """
    for row in range((size_n - 1), -1, -1):
        # Want grid to start at bottom left corner so we need the highest index for row
        # to be printed 'last', hence going from bottom to top.
        for column in range(0, size_n):
            if (isinstance(grid_tomorrow[(row, column)], int)) and (grid_tomorrow[(row, column)] >= 1):
                # Checks if the element in the grid is an integer and not a string. This is
                # necessary is because it is either a number to represent a healthy or infected
                # cell or it is an empty spot then it would be a '.' which is a string.
                # Then it checks if the current element is infected, if it is then increment.
                grid_tomorrow[(row, column)] += 1


def simulate_day(size_n, grid_today, chance_of_birth, chance_of_spread, duration_of_disease, rate_of_mortality):
    """
    Will simulate a day which includes the chance of the of new births, new infected,
    and infected death.

    Will create a new grid that will incorporate all the changes that will happen to the current
    grid. When the simulation is done, it will return the new grid that was created.
    """
    grid_tomorrow = grid_today.copy()

    # Increment infected counter in grid_tomorrow after copying
    increment_infected_duration(size_n, grid_tomorrow)

    # Birth new cells
    new_birth(size_n, grid_today, grid_tomorrow, chance_of_birth)

    # Infect cells
    new_infected(size_n, grid_today, grid_tomorrow, chance_of_spread)

    # Kill infected cells if old enough
    infected_mortality(size_n, grid_today, grid_tomorrow, rate_of_mortality, duration_of_disease)

    return grid_tomorrow


def completely_simulate(size_n, grid_today, chance_of_birth, chance_of_spread, duration_of_disease,
                        rate_of_mortality, simulate_for_days):
    """
    Will simulate for however many days specified by the user. This is done by calling simulate_days().
    """

    # Printing initial grid that was passed in
    print('\nStarting grid:')
    print_grid(size_n, grid_today)

    # Multiple simulation runs done here
    for days_past in range(1, simulate_for_days + 1):
        # It is simulate_for_days + 1 because it needs to be
        # offset or it wouldn't run the last day.
        print('\nGrid at the end of day {0} of the simulation:'.format(str(days_past)))
        tomorrow_grid = simulate_day(size_n, grid_today, chance_of_birth, chance_of_spread, duration_of_disease,
                                     rate_of_mortality)
        print_grid(size_n, tomorrow_grid)
        grid_today = tomorrow_grid


if __name__ == '__main__':
    grid_size = 20          # Represents n, the size for an n x n matrix.
    pop_density = 0.15      # Chance a square starts out as occupied.
    disease_chance = 0.1    # Chance to infect a healthy cell.

    birth_chance = 0.1      # Chance to birth a new healthy cell.
    spread_chance = 0.1     # Chance to spread disease to a healthy cell.
    disease_duration = 3    # How long a cell is infected for. Can be from 0 to 10.
    mortality_rate = 0.5    # Chance of infected cell dying.
    days = 500              # Number of days to simulate.

    # Initial grid generation
    today_grid = create_empty_grid(grid_size)
    initial_population(grid_size, today_grid, pop_density)
    initial_infected_population(grid_size, today_grid, disease_chance)

    completely_simulate(grid_size, today_grid, birth_chance, spread_chance, disease_duration,
                        mortality_rate, days)